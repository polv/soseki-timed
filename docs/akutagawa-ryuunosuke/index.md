# 芥川 竜之介

_あくたがわ りゅうのすけ_

生年：	1892-03-01
没年：	1927-07-24

人物について：	東大在学中に同人雑誌「新思潮」に発表した「鼻」を漱石が激賞し、文壇で活躍するようになる。王朝もの、近世初期のキリシタン文学、江戸時代の人物・事件、明治の文明開化期など、さまざまな時代の歴史的文献に題材をとり、スタイルや文体を使い分けたたくさんの短編小説を書いた。体力の衰えと「ぼんやりした不安」から自殺。その死は大正時代文学の終焉と重なっている。

「[芥川龍之介](https://ja.wikipedia.org/wiki/%E8%8A%A5%E5%B7%9D%E7%AB%9C%E4%B9%8B%E4%BB%8B)」
