export function makeSeconds(tStr: string) {
  const [min = "0", sec = tStr] = tStr.split(":");
  return Number(min) * 60 + Number(sec);
}
