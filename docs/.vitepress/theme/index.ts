import DefaultTheme from "vitepress/theme";
import { defineAsyncComponent } from "vue";
import { Theme } from "vitepress";
import "./index.scss";

const theme: Partial<Theme> = {
  extends: DefaultTheme,
  enhanceApp(ctx) {
    for (const [k, im] of Object.entries(
      import.meta.glob("./components/*.vue"),
    )) {
      const cName = /\/([^/]+)\.vue$/.exec(k)![1]!;
      ctx.app.component(cName, defineAsyncComponent(im as any));
    }
  },
};

export default theme;
