import { colorPlugin } from "markdown-it-color-plus";
import { defineConfig } from "vitepress";
import { tokenize } from "wakachigaki";

import furigana from "@patarapolw/furigana-markdown-it";
import { SearchPlugin } from "@patarapolw/vitepress-plugin-flexsearch";

export default defineConfig({
  title: "Home",
  titleTemplate: ":title - sosekiproject.org with audio timing",
  description: "sosekiproject.org with audio timing",
  lang: "ja",
  base: "/soseki-timed/",
  outDir: "../public",
  markdown: {
    // html: true,
    breaks: true,
    config(md) {
      md.use(furigana()).use(colorPlugin, {
        defaultClassName: "md-color",
        prefixClassName: "md-color-",
      });
    },
  },
  vite: {
    plugins: [
      SearchPlugin({
        encode: (s: string) => {
          return tokenize(s);
        },
        tokenize: "forward",
      } as any),
    ],
    server: {
      fs: {
        allow: ["../vitepress-plugin-search/dist"],
      },
    },
  },
  themeConfig: {
    outline: "deep",
    sidebar: [
      {
        text: "夏目 漱石",
        collapsed: false,
        link: "/natsume-soseki/",
        items: [
          {
            text: "こゝろ",
            collapsed: true,
            link: "/natsume-soseki/kokoro/",
            items: [
              {
                text: "(上) 先生と私",
                collapsed: true,
                items: [
                  {
                    text: "1-4",
                    link: "/natsume-soseki/kokoro/a-01-04",
                  },
                  {
                    text: "5-8",
                    link: "/natsume-soseki/kokoro/a-05-08",
                  },
                  {
                    text: "9-12",
                    link: "/natsume-soseki/kokoro/a-09-12",
                  },
                  {
                    text: "13-16",
                    link: "/natsume-soseki/kokoro/a-13-16",
                  },
                  {
                    text: "17-20",
                    link: "/natsume-soseki/kokoro/a-17-20",
                  },
                  {
                    text: "21-24",
                    link: "/natsume-soseki/kokoro/a-21-24",
                  },
                  {
                    text: "25-28",
                    link: "/natsume-soseki/kokoro/a-25-28",
                  },
                  {
                    text: "29-32",
                    link: "/natsume-soseki/kokoro/a-29-32",
                  },
                  {
                    text: "33-36",
                    link: "/natsume-soseki/kokoro/a-33-36",
                  },
                ],
              },
              {
                text: "(中) 両親と私",
                collapsed: true,
                items: [
                  {
                    text: "1-4",
                    link: "/natsume-soseki/kokoro/b-01-04",
                  },
                  {
                    text: "5-8",
                    link: "/natsume-soseki/kokoro/b-05-08",
                  },
                  {
                    text: "9-11",
                    link: "/natsume-soseki/kokoro/b-09-11",
                  },
                  {
                    text: "12-14",
                    link: "/natsume-soseki/kokoro/b-12-14",
                  },
                  {
                    text: "15-18",
                    link: "/natsume-soseki/kokoro/b-15-18",
                  },
                ],
              },
              {
                text: "(下) 先生と遺書",
                collapsed: true,
                items: [
                  {
                    text: "1-4",
                    link: "/natsume-soseki/kokoro/c-01-04",
                  },
                  {
                    text: "5-8",
                    link: "/natsume-soseki/kokoro/c-05-08",
                  },
                  {
                    text: "9-12",
                    link: "/natsume-soseki/kokoro/c-09-12",
                  },
                  {
                    text: "13-16",
                    link: "/natsume-soseki/kokoro/c-13-16",
                  },
                  {
                    text: "17-20",
                    link: "/natsume-soseki/kokoro/c-17-20",
                  },
                  {
                    text: "21-24",
                    link: "/natsume-soseki/kokoro/c-21-24",
                  },
                  {
                    text: "25-28",
                    link: "/natsume-soseki/kokoro/c-25-28",
                  },
                  {
                    text: "29-32",
                    link: "/natsume-soseki/kokoro/c-29-32",
                  },
                  {
                    text: "33-36",
                    link: "/natsume-soseki/kokoro/c-33-36",
                  },
                  {
                    text: "37-40",
                    link: "/natsume-soseki/kokoro/c-37-40",
                  },
                  {
                    text: "41-44",
                    link: "/natsume-soseki/kokoro/c-41-44",
                  },
                  {
                    text: "45-48",
                    link: "/natsume-soseki/kokoro/c-45-48",
                  },
                  {
                    text: "49-52",
                    link: "/natsume-soseki/kokoro/c-49-52",
                  },
                  {
                    text: "53-56",
                    link: "/natsume-soseki/kokoro/c-53-56",
                  },
                ],
              },
            ],
          },
          {
            text: "夢十夜",
            link: "/natsume-soseki/tendreams/",
            collapsed: true,
            items: [
              {
                text: "第一夜",
                link: "/natsume-soseki/tendreams/01",
              },
              {
                text: "第二夜",
                link: "/natsume-soseki/tendreams/02",
              },
              {
                text: "第三夜",
                link: "/natsume-soseki/tendreams/03",
              },
              {
                text: "第四夜",
                link: "/natsume-soseki/tendreams/04",
              },
              {
                text: "第五夜",
                link: "/natsume-soseki/tendreams/05",
              },
              {
                text: "第六夜",
                link: "/natsume-soseki/tendreams/06",
              },
              {
                text: "第七夜",
                link: "/natsume-soseki/tendreams/07",
              },
              {
                text: "第八夜",
                link: "/natsume-soseki/tendreams/08",
              },
              {
                text: "第九夜",
                link: "/natsume-soseki/tendreams/09",
              },
              {
                text: "第十夜",
                link: "/natsume-soseki/tendreams/10",
              },
            ],
          },
          {
            text: "吾輩は猫である",
            link: "/natsume-soseki/wagahai/",
            collapsed: true,
            items: [
              {
                text: "1",
                link: "/natsume-soseki/wagahai/01",
              },
              {
                text: "2",
                link: "/natsume-soseki/wagahai/02a",
                collapsed: true,
                items: [
                  {
                    text: "第2部",
                    link: "/natsume-soseki/wagahai/02b",
                  },
                ],
              },

              {
                text: "3",
                link: "/natsume-soseki/wagahai/03a",
                collapsed: true,
                items: [
                  {
                    text: "第2部",
                    link: "/natsume-soseki/wagahai/03b",
                  },
                ],
              },
              {
                text: "4",
                link: "/natsume-soseki/wagahai/04a",
                collapsed: true,
                items: [
                  {
                    text: "第2部",
                    link: "/natsume-soseki/wagahai/04b",
                  },
                ],
              },
              {
                text: "5",
                link: "/natsume-soseki/wagahai/05a",
                collapsed: true,
                items: [
                  {
                    text: "第2部",
                    link: "/natsume-soseki/wagahai/05b",
                  },
                ],
              },
              {
                text: "6",
                link: "/natsume-soseki/wagahai/06a",
                collapsed: true,
                items: [
                  {
                    text: "第2部",
                    link: "/natsume-soseki/wagahai/06b",
                  },
                ],
              },
              {
                text: "7",
                link: "/natsume-soseki/wagahai/07a",
                collapsed: true,
                items: [
                  {
                    text: "第2部",
                    link: "/natsume-soseki/wagahai/07b",
                  },
                ],
              },
              {
                text: "8",
                link: "/natsume-soseki/wagahai/08a",
                collapsed: true,
                items: [
                  {
                    text: "第2部",
                    link: "/natsume-soseki/wagahai/08b",
                  },
                ],
              },
              {
                text: "9",
                link: "/natsume-soseki/wagahai/09a",
                collapsed: true,
                items: [
                  {
                    text: "第2部",
                    link: "/natsume-soseki/wagahai/09b",
                  },
                ],
              },
              {
                text: "10",
                link: "/natsume-soseki/wagahai/10a",
                collapsed: true,
                items: [
                  {
                    text: "第2部",
                    link: "/natsume-soseki/wagahai/10b",
                  },
                ],
              },
              {
                text: "11",
                link: "/natsume-soseki/wagahai/11a",
                collapsed: true,
                items: [
                  {
                    text: "第2部",
                    link: "/natsume-soseki/wagahai/11b",
                  },
                  {
                    text: "第3部",
                    link: "/natsume-soseki/wagahai/11c",
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        text: "芥川 龍之介",
        link: "/akutagawa-ryuunosuke/",
        collapsed: false,
        items: [
          {
            text: "羅生門",
            link: "/akutagawa-ryuunosuke/rashoumon",
          },
        ],
      },
      {
        text: "小酒井 不木",
        link: "/kosakai-fuboku/",
        collapsed: false,
        items: [
          {
            text: "按摩",
            link: "/kosakai-fuboku/anma",
          },
        ],
      },
    ],
  },
});
