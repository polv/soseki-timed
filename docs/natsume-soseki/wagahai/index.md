---
head:
  - - meta
    - property: og:image
    - content: http://www.sosekiproject.org/iamacat/images/NekoPostcard1.jpg
next: 吾輩は猫である 1
---

# 吾輩は猫である

[吾輩]{わがはい}は[猫]{ねこ}である (1905)

A lost cat takes up residence in the home of a teacher, from where he offers a no-holds-barred commentary on his master, his master's household, the neighborhood, Japanese society, and human beings in general.

With audio by Fujii Naoko (藤井直子)

_Notes: this lengthy work is less a novel and more a series of episodes - it can as well be read in pieces as straight through; Chapter 1 was initially written as stand-alone - there is some discontinuity between Chapter 1 and the following chapters_
