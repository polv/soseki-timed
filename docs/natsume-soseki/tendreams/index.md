---
head:
  - - meta
    - property: og:image
    - content: https://www.sosekiproject.org/shortworks/images/tendreamscover200.jpg
prev: "こゝろ : (下) 先生と遺書"
---

# 夢十夜

[夢十夜]{ゆめ.じゅう.や} (1908)

A series of ten dreams set in various times and touching on various themes.

1st Night - 100 years' vigil
2nd night - the priest and the samurai
3rd night - child of stone
4th night - the old man with the snake
5th Night - racing against the dawn
6th night - rendering Niō
7th night - voyage to somewhere
8th night - the barber shop
9th night - a mother's story
10th night - the demise of Shōtarō

With audio by wis (ウィス)
