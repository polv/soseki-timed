---
head:
  - - meta
    - property: og:image
    - content: https://www.sosekiproject.org/kokoro/images/KokoroCoverLarge.jpg
next: (上) 先生と私 1-9
---

# こゝろ

[こゝろ]{こころ} (1914)

A young student forms a friendship with an enigmatic older man, whom he refers to as 'Sensei.' Over time, and finally through a long confessional letter, the younger man comes to know of Sensei's past and to understand the reasons behind his eccentric demeanor.

With audio by Watanabe Tomoake (渡辺知明)
