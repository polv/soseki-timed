---
head:
  - - meta
    - property: og:image
    - content: https://www.sosekiproject.org/kokoro/images/KokoroCoverLarge.jpg
---

# (上) 先生と私 1-4

<!-- section 001
study guide -->
## 一

私はその人を常に先生と呼んでいた。だからここでもただ先生と書くだけで本名は打ち明けない。これは世間を憚かる遠慮というよりも、その方が私にとって自然だからである。私はその人の記憶を呼び起すごとに、すぐ「先生」といいたくなる。筆を執っても心持は同じ事である。よそよそしい頭文字などはとても使う気にならない。

私が先生と知り合いになったのは鎌倉である。その時私はまだ若々しい書生であった。暑中休暇を利用して海水浴に行った友達からぜひ来いという端書を受け取ったので、私は多少の金を工面して、出掛ける事にした。私は金の工面に二、三日を費やした。ところが私が鎌倉に着いて三日と経たないうちに、私を呼び寄せた友達は、急に国元から帰れという電報を受け取った。電報には母が病気だからと断ってあったけれども友達はそれを信じなかった。友達はかねてから国元にいる親たちに勧まない結婚を強いられていた。彼は現代の習慣からいうと結婚するにはあまり年が若過ぎた。それに肝心の当人が気に入らなかった。それで夏休みに当然帰るべきところを、わざと避けて東京の近くで遊んでいたのである。彼は電報を私に見せてどうしようと相談をした。私にはどうしていいか分らなかった。けれども実際彼の母が病気であるとすれば彼は固より帰るべきはずであった。それで彼はとうとう帰る事になった。せっかく来た私は一人取り残された。

<!-- Section 002
study guide -->
学校の授業が始まるにはまだ大分日数があるので鎌倉におってもよし、帰ってもよいという境遇にいた私は、当分元の宿に留まる覚悟をした。友達は中国のある資産家の息子で金に不自由のない男であったけれども、学校が学校なのと年が年なので、生活の程度は私とそう変りもしなかった。したがって一人ぼっちになった私は別に恰好な宿を探す面倒ももたなかったのである。

宿は鎌倉でも辺鄙な方角にあった。玉突きだのアイスクリームだのというハイカラなものには長い畷を一つ越さなければ手が届かなかった。車で行っても二十銭は取られた。けれども個人の別荘はそこここにいくつでも建てられていた。それに海へはごく近いので海水浴をやるには至極便利な地位を占めていた。

私は毎日海へはいりに出掛けた。古い燻ぶり返った藁葺の間を通り抜けて磯へ下りると、この辺にこれほどの都会人種が住んでいるかと思うほど、避暑に来た男や女で砂の上が動いていた。ある時は海の中が銭湯のように黒い頭でごちゃごちゃしている事もあった。その中に知った人を一人ももたない私も、こういう賑やかな景色の中に裹まれて、砂の上に寝そべってみたり、膝頭を波に打たしてそこいらを跳ね廻るのは愉快であった。

私は実に先生をこの雑沓の間に見付け出したのである。その時海岸には掛茶屋が二軒あった。私はふとした機会からその一軒の方に行き慣れていた。長谷辺に大きな別荘を構えている人と違って、各自に専有の着換場を拵えていないここいらの避暑客には、ぜひともこうした共同着換所といった風なものが必要なのであった。彼らはここで茶を飲み、ここで休息する外に、ここで海水着を洗濯させたり、ここで鹹はゆい身体を清めたり、ここへ帽子や傘を預けたりするのである。海水着を持たない私にも持物を盗まれる恐れはあったので、私は海へはいるたびにその茶屋へ一切を脱ぎ棄てる事にしていた。

<!-- Section 003
study guide -->
## 二

私がその掛茶屋で先生を見た時は、先生がちょうど着物を脱いでこれから海へ入ろうとするところであった。私はその時反対に濡れた身体を風に吹かして水から上がって来た。二人の間には目を遮る幾多の黒い頭が動いていた。特別の事情のない限り、私はついに先生を見逃したかも知れなかった。それほど浜辺が混雑し、それほど私の頭が放漫であったにもかかわらず、私がすぐ先生を見付け出したのは、先生が一人の西洋人を伴れていたからである。

その西洋人の優れて白い皮膚の色が、掛茶屋へ入るや否や、すぐ私の注意を惹いた。純粋の日本の浴衣を着ていた彼は、それを床几の上にすぽりと放り出したまま、腕組みをして海の方を向いて立っていた。彼は我々の穿く猿股一つの外何物も肌に着けていなかった。私にはそれが第一不思議だった。私はその二日前に由井が浜まで行って、砂の上にしゃがみながら、長い間西洋人の海へ入る様子を眺めていた。私の尻をおろした所は少し小高い丘の上で、そのすぐ傍がホテルの裏口になっていたので、私の凝としている間に、大分多くの男が塩を浴びに出て来たが、いずれも胴と腕と股は出していなかった。女は殊更肉を隠しがちであった。大抵は頭に護謨製の頭巾を被って、海老茶や紺や藍の色を波間に浮かしていた。そういう有様を目撃したばかりの私の眼には、猿股一つで済まして皆なの前に立っているこの西洋人がいかにも珍しく見えた。

彼はやがて自分の傍を顧みて、そこにこごんでいる日本人に、一言二言何かいった。その日本人は砂の上に落ちた手拭を拾い上げているところであったが、それを取り上げるや否や、すぐ頭を包んで、海の方へ歩き出した。その人がすなわち先生であった。

<!-- Section 004
study guide -->
私は単に好奇心のために、並んで浜辺を下りて行く二人の後姿を見守っていた。すると彼らは真直に波の中に足を踏み込んだ。そうして遠浅の磯近くにわいわい騒いでいる多人数の間を通り抜けて、比較的広々した所へ来ると、二人とも泳ぎ出した。彼らの頭が小さく見えるまで沖の方へ向いて行った。それから引き返してまた一直線に浜辺まで戻って来た。掛茶屋へ帰ると、井戸の水も浴びずに、すぐ身体を拭いて着物を着て、さっさとどこへか行ってしまった。

彼らの出て行った後、私はやはり元の床几に腰をおろして烟草を吹かしていた。その時私はぽかんとしながら先生の事を考えた。どうもどこかで見た事のある顔のように思われてならなかった。しかしどうしてもいつどこで会った人か想い出せずにしまった。

その時の私は屈托がないというよりむしろ無聊に苦しんでいた。それで翌日もまた先生に会った時刻を見計らって、わざわざ掛茶屋まで出かけてみた。すると西洋人は来ないで先生一人麦藁帽を被ってやって来た。先生は眼鏡をとって台の上に置いて、すぐ手拭で頭を包んで、すたすた浜を下りて行った。先生が昨日のように騒がしい浴客の中を通り抜けて、一人で泳ぎ出した時、私は急にその後が追い掛けたくなった。私は浅い水を頭の上まで跳かして相当の深さの所まで来て、そこから先生を目標に抜手を切った。すると先生は昨日と違って、一種の弧線を描いて、妙な方向から岸の方へ帰り始めた。それで私の目的はついに達せられなかった。私が陸へ上がって雫の垂れる手を振りながら掛茶屋に入ると、先生はもうちゃんと着物を着て入れ違いに外へ出て行った。

<!-- Section 005
study guide -->
## 三

私は次の日も同じ時刻に浜へ行って先生の顔を見た。その次の日にもまた同じ事を繰り返した。けれども物をいい掛ける機会も、挨拶をする場合も、二人の間には起らなかった。その上先生の態度はむしろ非社交的であった。一定の時刻に超然として来て、また超然と帰って行った。周囲がいくら賑やかでも、それにはほとんど注意を払う様子が見えなかった。最初いっしょに来た西洋人はその後まるで姿を見せなかった。先生はいつでも一人であった。

或る時先生が例の通りさっさと海から上がって来て、いつもの場所に脱ぎ棄てた浴衣を着ようとすると、どうした訳か、その浴衣に砂がいっぱい着いていた。先生はそれを落すために、後ろ向きになって、浴衣を二、三度振った。すると着物の下に置いてあった眼鏡が板の隙間から下へ落ちた。先生は白絣の上へ兵児帯を締めてから、眼鏡の失くなったのに気が付いたと見えて、急にそこいらを探し始めた。私はすぐ腰掛の下へ首と手を突ッ込んで眼鏡を拾い出した。先生は有難うといって、それを私の手から受け取った。

次の日私は先生の後につづいて海へ飛び込んだ。そうして先生といっしょの方角に泳いで行った。二丁ほど沖へ出ると、先生は後ろを振り返って私に話し掛けた。広い蒼い海の表面に浮いているものは、その近所に私ら二人より外になかった。そうして強い太陽の光が、眼の届く限り水と山とを照らしていた。私は自由と歓喜に充ちた筋肉を動かして海の中で躍り狂った。先生はまたぱたりと手足の運動を已めて仰向けになったまま浪の上に寝た。私もその真似をした。青空の色がぎらぎらと眼を射るように痛烈な色を私の顔に投げ付けた。「愉快ですね」と私は大きな声を出した。

<!-- Section 006
study guide -->
しばらくして海の中で起き上がるように姿勢を改めた先生は、「もう帰りませんか」といって私を促した。比較的強い体質をもった私は、もっと海の中で遊んでいたかった。しかし先生から誘われた時、私はすぐ「ええ帰りましょう」と快く答えた。そうして二人でまた元の路を浜辺へ引き返した。

私はこれから先生と懇意になった。しかし先生がどこにいるかはまだ知らなかった。

それから中二日おいてちょうど三日目の午後だったと思う。先生と掛茶屋で出会った時、先生は突然私に向かって、「君はまだ大分長くここにいるつもりですか」と聞いた。考えのない私はこういう問いに答えるだけの用意を頭の中に蓄えていなかった。それで「どうだか分りません」と答えた。しかしにやにや笑っている先生の顔を見た時、私は急に極りが悪くなった。「先生は？」と聞き返さずにはいられなかった。これが私の口を出た先生という言葉の始まりである。

私はその晩先生の宿を尋ねた。宿といっても普通の旅館と違って、広い寺の境内にある別荘のような建物であった。そこに住んでいる人の先生の家族でない事も解った。私が先生先生と呼び掛けるので、先生は苦笑いをした。私はそれが年長者に対する私の口癖だといって弁解した。私はこの間の西洋人の事を聞いてみた。先生は彼の風変りのところや、もう鎌倉にいない事や、色々の話をした末、日本人にさえあまり交際をもたないのに、そういう外国人と近付きになったのは不思議だといったりした。私は最後に先生に向かって、どこかで先生を見たように思うけれども、どうしても思い出せないといった。若い私はその時暗に相手も私と同じような感じを持っていはしまいかと疑った。そうして腹の中で先生の返事を予期してかかった。ところが先生はしばらく沈吟したあとで、「どうも君の顔には見覚えがありませんね。人違いじゃないですか」といったので私は変に一種の失望を感じた。

<!-- Section 007
study guide -->
## 四

私は月の末に東京へ帰った。先生の避暑地を引き上げたのはそれよりずっと前であった。私は先生と別れる時に、「これから折々お宅へ伺っても宜ござんすか」と聞いた。先生は単簡にただ「ええいらっしゃい」といっただけであった。その時分の私は先生とよほど懇意になったつもりでいたので、先生からもう少し濃かな言葉を予期して掛ったのである。それでこの物足りない返事が少し私の自信を傷めた。

私はこういう事でよく先生から失望させられた。先生はそれに気が付いているようでもあり、また全く気が付かないようでもあった。私はまた軽微な失望を繰り返しながら、それがために先生から離れて行く気にはなれなかった。むしろそれとは反対で、不安に揺かされるたびに、もっと前へ進みたくなった。もっと前へ進めば、私の予期するあるものが、いつか眼の前に満足に現われて来るだろうと思った。私は若かった。けれどもすべての人間に対して、若い血がこう素直に働こうとは思わなかった。私はなぜ先生に対してだけこんな心持が起るのか解らなかった。それが先生の亡くなった今日になって、始めて解って来た。先生は始めから私を嫌っていたのではなかったのである。先生が私に示した時々の素気ない挨拶や冷淡に見える動作は、私を遠ざけようとする不快の表現ではなかったのである。傷ましい先生は、自分に近づこうとする人間に、近づくほどの価値のないものだから止せという警告を与えたのである。他の懐かしみに応じない先生は、他を軽蔑する前に、まず自分を軽蔑していたものとみえる。

私は無論先生を訪ねるつもりで東京へ帰って来た。帰ってから授業の始まるまでにはまだ二週間の日数があるので、そのうちに一度行っておこうと思った。しかし帰って二日三日と経つうちに、鎌倉にいた時の気分が段々薄くなって来た。そうしてその上に彩られる大都会の空気が、記憶の復活に伴う強い刺戟と共に、濃く私の心を染め付けた。私は往来で学生の顔を見るたびに新しい学年に対する希望と緊張とを感じた。私はしばらく先生の事を忘れた。

<!-- Section 008
study guide -->
授業が始まって、一カ月ばかりすると私の心に、また一種の弛みができてきた。私は何だか不足な顔をして往来を歩き始めた。物欲しそうに自分の室の中を見廻した。私の頭には再び先生の顔が浮いて出た。私はまた先生に会いたくなった。

始めて先生の宅を訪ねた時、先生は留守であった。二度目に行ったのは次の日曜だと覚えている。晴れた空が身に沁み込むように感ぜられる好い日和であった。その日も先生は留守であった。鎌倉にいた時、私は先生自身の口から、いつでも大抵宅にいるという事を聞いた。むしろ外出嫌いだという事も聞いた。二度来て二度とも会えなかった私は、その言葉を思い出して、理由もない不満をどこかに感じた。私はすぐ玄関先を去らなかった。下女の顔を見て少し躊躇してそこに立っていた。この前名刺を取り次いだ記憶のある下女は、私を待たしておいてまた内へはいった。すると奥さんらしい人が代って出て来た。美しい奥さんであった。

私はその人から鄭寧に先生の出先を教えられた。先生は例月その日になると雑司ヶ谷の墓地にある或る仏へ花を手向けに行く習慣なのだそうである。「たった今出たばかりで、十分になるか、ならないかでございます」と奥さんは気の毒そうにいってくれた。私は会釈して外へ出た。賑かな町の方へ一丁ほど歩くと、私も散歩がてら雑司ヶ谷へ行ってみる気になった。先生に会えるか会えないかという好奇心も動いた。それですぐ踵を回らした。
