import { load as cheerio } from "cheerio";
import { readFileSync, writeFileSync } from "fs";
import https from "https";
import { fileURLToPath } from "url";
import XLSX from "xlsx";

interface IVocab {
  id: string;
  vocab: string;
  reading: string;
  meaning: string;
}

async function getVocab(url: string) {
  const $ = cheerio(await httpsGet(url));
  const out: IVocab[] = [];

  $(".vocab_list .vocabdef").each(function () {
    const splitter = "\u00a0";

    const $el = $(this);

    const [id, meaningHTML] = ($el.html() || "").split("&nbsp;");

    const vocab = (() => {
      const $it = $el.clone();
      $it.find("rt").text("");
      return $it.text().split(splitter)[0];
    })();
    const reading = (() => {
      const $it = $el.clone();
      $it.find("rb").text("");
      return $it.text().split(splitter)[0];
    })();

    out.push({
      id,
      vocab,
      reading,
      meaning: $("<div>").html(meaningHTML).text(),
    });
  });

  return out;
}

async function httpsGet(url: string) {
  return new Promise<string>((resolve, reject) => {
    https.get(url, (res) => {
      if (res.statusCode !== 200) {
        reject(res.statusCode);
      }

      const all: string[] = [];
      res.on("data", (chuck: Buffer) => all.push(chuck.toString()));
      res.once("end", () => {
        resolve(all.join(""));
      });
      res.once("error", reject);
    });
  });
}

function showUnicode(s: string) {
  return JSON.stringify(s).replace(/[\u007F-\uFFFF]/g, function (chr) {
    return "\\u" + ("0000" + chr.charCodeAt(0).toString(16)).substring(-4);
  });
}

async function main() {
  const md = readFileSync("docs\\natsume-soseki\\wagahai\\02a.md", "utf-8");

  const output = new Map<string, IVocab[]>();

  const re = /\<AudioWide.*href="(.+?)".*\>/g;
  let m: RegExpExecArray | null = null;

  while ((m = re.exec(md))) {
    console.log(m[1]);
    for (const it of await getVocab(m[1])) {
      const ls = output.get(it.id) || [];
      ls.push(it);
      output.set(it.id, ls);
    }
  }

  const wb = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(
    wb,
    XLSX.utils.aoa_to_sheet(
      Array.from(output.values())
        .sort((a, b) => b.length - a.length)
        .map(([v]) => [v.vocab, v.reading, v.meaning]),
    ),
  );
  XLSX.writeFile(wb, "out/vocab.xlsx");
}

if (process.argv[1] === fileURLToPath(import.meta.url)) {
  await main();
}
